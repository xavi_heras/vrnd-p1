﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CarnivalEntrance : MonoBehaviour {

	void Start() {		
		Camera.main.GetComponent<ScreenFade>().FadeToClear ();
	}

	void Update(){
		if (Input.GetKeyDown (KeyCode.I))
			Camera.main.GetComponent<ScreenFade>().FadeToColor ();
		if (Input.GetKeyDown (KeyCode.O))
			Camera.main.GetComponent<ScreenFade>().FadeToClear ();
		if (Input.GetKeyDown (KeyCode.L))
			SceneManager.LoadScene ("Carnival");
	}

	public void EnterCarnival() {
		Camera.main.GetComponent<ScreenFade>().FadeToColor ();
		StartCoroutine ("LoadCarnival");
	}


	IEnumerator LoadCarnival(){
		yield return new WaitForSeconds (Camera.main.GetComponent<ScreenFade> ().FadeTime);
		SceneManager.LoadScene ("Carnival");
	}

}
