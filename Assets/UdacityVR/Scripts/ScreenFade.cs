﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ScreenFade : MonoBehaviour {

	[Tooltip("Fade in/out speed")]
	public float FadeTime = .5f;

	void Start() {
		
	}
		
	public void FadeToClear() {		
		iTween.FadeTo (GameObject.Find("Fade").gameObject, 0f, FadeTime);
	}

	public void FadeToColor() {
		iTween.FadeTo (GameObject.Find("Fade").gameObject, 1f, FadeTime);
	}

}
